/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, _ */

 OB.UTIL.HookManager.registerHook('OBPOS_TerminalLoadedFromBackend', function (args, callbacks) {
   new OB.DS.Process('com.openbravo.sharaf.integration.retail.rfidsn.service.GetRFIDAPIPath').exec({
        org: OB.MobileApp.model.get('terminal').organization
    }, function(data) {
        if (data && data.exception) {
            if (!OB.UTIL.isNullOrUndefined(data.exception.message)) {
                console.log(data.exception.message);
            }
        } else {
            var response = JSON.parse(JSON.stringify(data));
            localStorage.setItem('RFIDApiPath',  response.RFIDPath);
            OB.UTIL.HookManager.callbackExecutor(args, callbacks);
        }
    });
});
            