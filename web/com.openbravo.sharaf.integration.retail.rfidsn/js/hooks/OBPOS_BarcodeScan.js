OB.UTIL.HookManager.registerHook('OBPOS_BarcodeScan', function(
  args,
  callbacks
) {
     var rfidapiPath = localStorage.getItem('RFIDApiPath');
     var epccode ='';
    
    var splittedScan = args.code.split(',');  // Or any other character that separates the info
    if (splittedScan.length === 2 && OB.MobileApp.model.get('terminal').custrfdEnablerfid) {
    // if the split function actually found a comma, then it's an RFID tag and not a normal barcode or keyboard product
    // if the splittedScan has length 1, then it's a normal product and will skip this hook
    
    // We modify the args that will continue through the hooks and will finally be added
        args.attrs = args.attrs || {};
        args.attrs.obposEpccode = splittedScan[1];
        args.code = splittedScan[0]; 
        epccode =  splittedScan[1];
        
    rfidapiPath = rfidapiPath + epccode;
       
    if (epccode !='' && !OB.CUSTRFD.Utils.isValidRFID(epccode)) {
        OB.UTIL.showConfirmation.display(OB.I18N.getLabel('CUSTRFD_LblError'), OB.I18N.getLabel('CUSTRFD_LblErrorRfid'));
        return;
    }  
     if(!OB.UTIL.isNullOrUndefined(rfidapiPath) && rfidapiPath != '' && epccode != '' ) {
       var query = "select p.CustrfdIsSerialnoEnabled,p.searchkey from m_product p where p.CustrfdIsSerialnoEnabled = 'true' and "
            + " p.m_product_id in (select u.m_product_id from obmupc_prod_multiupc u where u.upc = '" + args.code + "')" ; 
            
       OB.Dal.queryUsingCache(OB.Model.Product, query, [], function(success) { 
       if(success.length > 0 ) {
         barcodeRequest = {
        method: 'GET',
        contenttype: 'application/json;charset=utf-8',
        headers: {
               Accept: '*/*'
        },
        url :rfidapiPath
        }
        return new Promise(function(resolve, reject) {
             var url = OB.POS.hwserver.url;
             if (url) {
                 var ajax = new enyo.Ajax({
                 url: url.replace('/printer', '/httpproxy'),
                 cacheBust: false,
                 method: 'POST',
                 handleAs: 'json',
                 contentType: 'application/json;charset=utf-8',
                 Accept: '*/*'
              }).go(JSON.stringify(barcodeRequest));
                 ajax.response(function(inSender, inResponse) {
                   if(!OB.UTIL.isNullOrUndefined(inResponse) && !OB.UTIL.isNullOrUndefined(inResponse.content) 
                     && inResponse.content != 'No record found\r\n' ) {
                     var res = JSON.parse(inResponse.content);
                     args.attrs.obposSerialNumber = res.serial;
                     args.attrs.obposEpccode = res.epc;
                     OB.UTIL.HookManager.callbackExecutor(args, callbacks); 
                   } else {
                      if(inResponse.content == 'No record found\r\n')  {
                              OB.UTIL.showError('Record Not Found');
                             // OB.UTIL.HookManager.callbackExecutor(args, callbacks); 
                      } else {
                             OB.UTIL.showError('SerialNo Not Found');
                             OB.UTIL.HookManager.callbackExecutor(args, callbacks); 
                      }
                   }
                 });
                 ajax.error(function(inSender, inResponse) {
                   OB.UTIL.showError('Error while Procesing RFID API request');  
                   //OB.UTIL.HookManager.callbackExecutor(args, callbacks); 
                   OB.error('Error Procesing RFID API request: ' + inResponse);
                 });
            } else {
                  OB.UTIL.showError('RFID API Call Rejected'); 
                  OB.error('RFID API Call Rejected');
             reject({
                response: {},
                message: 'RFID API Configuration Error'
             });
           }
         });  
       } else {
           OB.UTIL.HookManager.callbackExecutor(args, callbacks); 
       }
       });
           
     } else {
        OB.UTIL.HookManager.callbackExecutor(args, callbacks); 
     }
    } else {
        OB.UTIL.HookManager.callbackExecutor(args, callbacks); 
    }
    
  // We continue the normal execution of the rest of the hooks
});









