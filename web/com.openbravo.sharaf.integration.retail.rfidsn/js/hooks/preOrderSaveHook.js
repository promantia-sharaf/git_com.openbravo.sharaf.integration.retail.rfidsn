/*
 ************************************************************************************
 * Copyright (C) 2017-2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global _, $, CUSTRFD, enyo*/

(function () {

  OB = OB || {};

  OB.CUSTRFDUTIL = OB.CUSTRFDUTIL || {};

  OB.CUSTRFDUTIL.custrfdFindCategory = function (categoryId, line, brand, sGroup, department, documentNo, resultMsg, rfidLinesChecked) {
    OB.Dal.find(OB.Model.ProductCategory, {
      id: categoryId
    }, function (categories) {
      if (categories.length === 1) {
        if (OB.UTIL.isNullOrUndefined(sGroup)) {
          sGroup = categories.at(0).get('name');
        }
        if (categories.at(0).get('custshaCategoryType') === 'Department') {
          department = categories.at(0).get('name');
          OB.CUSTRFDUTIL.rfidSnCall(line, brand, sGroup, department, documentNo, resultMsg, rfidLinesChecked);
        } else {
          OB.CUSTRFDUTIL.custrfdFindParentId(categories.at(0).get('id'), line, brand, sGroup, department, documentNo, resultMsg, rfidLinesChecked);
        }
      } else {
        //TODO Throw Error
        resultMsg.push({
          content: OB.I18N.getLabel('CUSTRFD_ErrorInCategoryTree') + line.get('product').get('_identifier') + '.\n'
        });
        rfidLinesChecked();
      }
    });
  };

  OB.CUSTRFDUTIL.custrfdFindParentId = function (categoryId, line, brand, sGroup, department, documentNo, resultMsg, rfidLinesChecked) {
    OB.Dal.find(OB.Model.ProductCategoryTree, {
      categoryId: categoryId
    }, function (categories) {
      if (categories.length === 1) {
        var parent = categories.at(0).get('parentId');
        var parentCategory = OB.CUSTRFDUTIL.custrfdFindCategory(parent, line, brand, sGroup, department, documentNo, resultMsg, rfidLinesChecked);
      } else {
        //TODO Throw Error
        resultMsg.push({
          content: OB.I18N.getLabel('CUSTRFD_ErrorInCategoryTree') + line.get('product').get('_identifier') + '.\n'
        });
        rfidLinesChecked();
      }
    });
  };
  OB.CUSTRFDUTIL.rfidSnCall = function (line, brand, sGroup, department, documentNo, resultMsg, rfidLinesChecked) {
    var ajaxRequest = new enyo.Ajax({
      url: OB.MobileApp.model.get('terminal').mconPath,
      headers: {
        accept: 'application/json',
        api_key: OB.MobileApp.model.get('terminal').mconAPIKey
      },
      method: 'POST',
      dataType: 'json',
      cacheBust: 'false',
      contentType: 'application/json; charset=utf-8',
      postBody: JSON.stringify({
        facilityCode: OB.MobileApp.model.get('terminal').custrfdOrgSearchKey,
        thingTypeCode: 'sharaf.rfid',
        Timestamp: new Date(),
        tagID: line.get('obposEpccode'),
        itemcode: line.get('product').get('searchkey'),
        supplier: '',
        productDescrip: line.get('product').get('description'),
        sGroup: sGroup,
        Department: department,
        StockType: OB.UTIL.isNullOrUndefined(line.get('cUSTSHAStockType')) ? 'N' : line.get('cUSTSHAStockType'),
        DocumentNum: documentNo,
        brand: brand,
        serialNUM: line.get('obposSerialNumber'),
        status: line.getQty() > 0 ? 'Sold' : 'GRN'
      }),
      success: function (inSender, inResponse) {
        rfidLinesChecked();
      },
      fail: function (inSender, inResponse) {
        if(!OB.MobileApp.model.get('terminal').custrfdEnablerfid) {
          resultMsg.push({
          content: OB.I18N.getLabel('CUSTRFD_MCON_ServerError') + line.get('product').get('_identifier') + '.\n'
        });
        }  
          
        rfidLinesChecked();
      }
    });
    ajaxRequest.go().response('success').error('fail');
  };
  OB.UTIL.HookManager.registerHook('OBPOS_PreOrderSave', function (args, callbacks) {
    var receipt = args.receipt,
        documentNo = args.receipt.get('documentNo'),
        resultMsg = [{
        allowHtml: true,
        content: '&nbsp;'
      }],
        error = false,
        rfidLinesChecked, rfidReturnResult;

    if (!OB.MobileApp.model.get('terminal').custrfdOrgIsRfidEnabled || receipt.get('isQuotation') || receipt.isLayaway()) {
      OB.UTIL.HookManager.callbackExecutor(args, callbacks);
      return;
    }

    var rfidLines = _.filter(receipt.get('lines').models, function (line) {
      return line.has('obposEpccode') && !OB.UTIL.isNullOrUndefined(line.get('obposEpccode')) && line.get('obposEpccode') !== '';
    });

    if (!rfidLines.length) {
      OB.UTIL.HookManager.callbackExecutor(args, callbacks);
      return;
    }

    rfidReturnResult = function () {
      if (resultMsg.length > 1) {
        OB.UTIL.showConfirmation.display(OB.I18N.getLabel('CUSTRFD_RFIDInfoSendingError'), resultMsg);
      } else {
        OB.UTIL.showSuccess(OB.I18N.getLabel('CUSTRFD_RFIDInfoSuccessfullySent'));
      }
    };

    rfidLinesChecked = _.after(rfidLines.length, rfidReturnResult);

    _.each(rfidLines, function (line) {
      var brand = null,
          sGroup = null,
          department = null;
      OB.Dal.find(OB.Model.Brand, {
        id: line.get('product').get('brand')
      }, function (brand) {
        if (brand.length === 1) {
          brand = brand.at(0).get('name');
        }
        OB.CUSTRFDUTIL.custrfdFindCategory(line.get('product').get('productCategory'), line, brand, sGroup, department, documentNo, resultMsg, rfidLinesChecked);
      });
    });
    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
  });

}());