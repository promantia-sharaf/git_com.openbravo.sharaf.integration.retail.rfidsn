/*
 ************************************************************************************
 * Copyright (C) 2017-2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB */

OB.UTIL.HookManager.registerHook('OBPOS_PreAddProductToOrder', function (args, callbacks) {
  
  if(OB.MobileApp.model.get('terminal').custrfdEnablerfid) {
      var negativeLines = [];
      var lines =  args.receipt.get('lines');
      var rfid = OB.UTIL.isNullOrUndefined(args.attrs.obposEpccode) ? '' : args.attrs.obposEpccode;  
  
      if (rfid != '' && lines.length !== 0 && !OB.CUSTRFD.Utils.isUniqueRFIDImpl(rfid, lines)) {
            OB.UTIL.showConfirmation.display(OB.I18N.getLabel('CUSTRFD_LblError'), OB.I18N.getLabel('CUSTRFD_UniqueErrorRfid'));
            return;
      } else if (args.productToAdd.get('custrfdIsSerialnoEnabled') && (args.productToAdd.get('custrfdIsRfidEnabled') && OB.MobileApp.model.get('terminal').custrfdOrgIsRfidEnabled)
             && !OB.UTIL.isNullOrUndefined(args.attrs.obposEpccode) && !OB.UTIL.isNullOrUndefined(args.attrs.obposSerialNumber)
             && args.attrs.obposEpccode != '' && args.attrs.obposSerialNumber != '' ) {
         negativeLines = _.filter(args.receipt.get('lines').models, function (line) {
          if( line.get('product').attributes.searchkey === args.productToAdd.attributes.searchkey 
             && (OB.UTIL.isNullOrUndefined(line.get('obposEpccode')) || line.get('obposEpccode') === '' )
            && (OB.UTIL.isNullOrUndefined(line.get('obposSerialNumber')) || line.get('obposSerialNumber') === '' )){
            return line; 
          }
        });
        
      } else if(!args.productToAdd.get('custrfdIsSerialnoEnabled') && args.productToAdd.get('custrfdIsRfidEnabled') 
                   && !OB.UTIL.isNullOrUndefined(args.attrs.obposEpccode) && args.attrs.obposEpccode != '') {
        var negativeLines = _.filter(args.receipt.get('lines').models, function (line) {
          if(line.get('product').attributes.searchkey === args.productToAdd.attributes.searchkey
           && (OB.UTIL.isNullOrUndefined(line.get('obposEpccode')) || line.get('obposEpccode') === '' )) {
            return line; 
          }
        });
      }
      if(negativeLines.length > 0) {
        negativeLines[0].set('obposEpccode',args.attrs.obposEpccode);
        negativeLines[0].set('obposSerialNumber',args.attrs.obposSerialNumber);
        negativeLines[0].set('obposRfidValid',true);
        
        args.receipt.save();
        args.attrs = null;
        args.productToAdd = null;  
        args.qtyToAdd = 0;
        args.cancellation = true;
        return;
      } else {
        OB.UTIL.HookManager.callbackExecutor(args, callbacks);
      }
  } else {
      OB.UTIL.HookManager.callbackExecutor(args, callbacks);
  }
  
});
