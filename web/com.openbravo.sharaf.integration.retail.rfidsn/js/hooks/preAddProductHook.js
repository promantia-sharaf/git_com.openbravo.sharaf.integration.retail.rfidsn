/*
 ************************************************************************************
 * Copyright (C) 2017-2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB */

OB.UTIL.HookManager.registerHook('OBPOS_PreAddProductToOrder', function (args, callbacks) {
  if (args.cancelOperation) {
    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
    return;
  }
  if(OB.MobileApp.model.get('terminal').custrfdEnablerfid && OB.CUSTRFD.Utils.isValidDocumentType(OB.MobileApp.model.receipt.get('custsdtDocumenttypeSearchKey')) && args.productToAdd.get('groupProduct') === false) {
    if (args.productToAdd.get('cUSTDELDeliveryCondition') !== 'SD' 
            && args.productToAdd.get('cUSTDELDeliveryCondition') !== 'WD' 
            && args.productToAdd.get('cUSTDELDeliveryCondition') !== 'C' 
            && args.productToAdd.get('cUSTDELDeliveryCondition') !== 'ON'
            && args.productToAdd.get('cUSTDELDeliveryCondition') !== 'P') {
      if (args.productToAdd.get('custrfdIsSerialnoEnabled') && (args.productToAdd.get('custrfdIsRfidEnabled') && OB.MobileApp.model.get('terminal').custrfdOrgIsRfidEnabled)
         && !OB.UTIL.isNullOrUndefined(args.attrs.obposEpccode) && !OB.UTIL.isNullOrUndefined(args.attrs.obposSerialNumber)
         && args.attrs.obposEpccode != '' && args.attrs.obposSerialNumber != '' ) {
            OB.UTIL.HookManager.callbackExecutor(args, callbacks); 
       } else if(!args.productToAdd.get('custrfdIsSerialnoEnabled') && args.productToAdd.get('custrfdIsRfidEnabled') 
               && !OB.UTIL.isNullOrUndefined(args.attrs.obposEpccode) && args.attrs.obposEpccode != '') {
             OB.UTIL.HookManager.callbackExecutor(args, callbacks);       
      } else {
        OB.CUSTRFD.Utils.addSerialNoRfidLine(args, callbacks);
      }
    } else {
      OB.UTIL.HookManager.callbackExecutor(args, callbacks);
    }
  } else if (OB.CUSTRFD.Utils.isValidDocumentType(OB.MobileApp.model.receipt.get('custsdtDocumenttypeSearchKey')) && args.productToAdd.get('groupProduct') === false) {
       if (args.productToAdd.get('cUSTDELDeliveryCondition') !== 'SD' 
                    && args.productToAdd.get('cUSTDELDeliveryCondition') !== 'WD' 
                    && args.productToAdd.get('cUSTDELDeliveryCondition') !== 'C' 
                    && args.productToAdd.get('cUSTDELDeliveryCondition') !== 'ON'
                    && args.productToAdd.get('cUSTDELDeliveryCondition') !== 'P') {
          if (args.productToAdd.get('custrfdIsSerialnoEnabled') || (args.productToAdd.get('custrfdIsRfidEnabled') && OB.MobileApp.model.get('terminal').custrfdOrgIsRfidEnabled)) {
            OB.CUSTRFD.Utils.addSerialNoRfidLine(args, callbacks);
          } else {
            OB.UTIL.HookManager.callbackExecutor(args, callbacks);
          }
     } else {
          OB.UTIL.HookManager.callbackExecutor(args, callbacks);
     }
} else {
  OB.UTIL.HookManager.callbackExecutor(args, callbacks);  
}
 
});
