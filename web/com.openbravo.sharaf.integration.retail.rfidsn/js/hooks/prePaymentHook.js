/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, _ */

OB.UTIL.HookManager.registerHook('OBPOS_PrePaymentHook', function (args, callbacks) {
  
  
  if(OB.MobileApp.model.get('terminal').custrfdEnablerfid) {
      var lineQueue = [],
      productMsg = [],
      order = args.context.get('order');
      productMsg.push('Please do RFID Scan for this product \n');
    if (order.get('isEditable') && OB.CUSTRFD.Utils.isValidDocumentType(order.get('custsdtDocumenttypeSearchKey'))) {
    _.each(order.get('lines').models, function (line) {
      if (!line.get('obposRfidValid')) {
        var product = line.get('product');
        if (line.get('cUSTDELDeliveryCondition') !== 'SD' && line.get('cUSTDELDeliveryCondition') !== 'WD' && line.get('cUSTDELDeliveryCondition') !== 'C' && line.get('cUSTDELDeliveryCondition') !== 'ON'
              && line.get('cUSTDELDeliveryCondition') !== 'P') {
         if (product.get('groupProduct') === false && ((product.get('custrfdIsRfidEnabled') 
               && OB.MobileApp.model.get('terminal').custrfdOrgIsRfidEnabled) && product.get('custrfdIsSerialnoEnabled'))) {
             if((OB.UTIL.isNullOrUndefined(line.get('obposEpccode')) || line.get('obposEpccode') === '')
                     && (OB.UTIL.isNullOrUndefined(line.get('obposSerialNumber')) || line.get('obposSerialNumber') === '')) { 
               var searchkey = product.get('searchkey');
               var productname = product.get('description');
               productMsg.push('Product : ' + ' ' + searchkey + ' ' + productname + ' \n' ); 
             }                     
         } else if(product.get('groupProduct') === false && product.get('custrfdIsRfidEnabled') && OB.MobileApp.model.get('terminal').custrfdOrgIsRfidEnabled) {
              if(OB.UTIL.isNullOrUndefined(line.get('obposEpccode')) || line.get('obposEpccode') === '') {
                  var searchkey = product.get('searchkey');
                  var productname = product.get('description');
                  productMsg.push('Product : ' + ' ' + searchkey + ' ' + productname + ' \n' ); 
              }          
         }             
     }
   }
    });
  }
   if (productMsg.length > 1) {
     args.cancellation = true;
     OB.UTIL.showConfirmation.display('',productMsg);
  } else {
    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
  }
  } else {
      var lineQueue = [],
      order = args.context.get('order');
     if (order.get('isEditable') && OB.CUSTRFD.Utils.isValidDocumentType(order.get('custsdtDocumenttypeSearchKey'))) {
    _.each(order.get('lines').models, function (line) {
      if (!line.get('obposRfidValid')) {
        var product = line.get('product');
        if (product.get('groupProduct') === false && ((product.get('custrfdIsRfidEnabled') && OB.MobileApp.model.get('terminal').custrfdOrgIsRfidEnabled) || product.get('custrfdIsSerialnoEnabled'))) {
          if (line.get('cUSTDELDeliveryCondition') !== 'SD' && line.get('cUSTDELDeliveryCondition') !== 'WD' && line.get('cUSTDELDeliveryCondition') !== 'C' && line.get('cUSTDELDeliveryCondition') !== 'ON'
              && line.get('cUSTDELDeliveryCondition') !== 'P') {
            lineQueue.push({
              line: line,
              checked: false
            });
          }
        } else {
          line.set('obposRfidValid', true);
        }
      }
    });
  }
  if (lineQueue.length > 0) {
    OB.CUSTRFD.Utils.checkSerialNoRfidLines(order, lineQueue, function () {
      OB.UTIL.HookManager.callbackExecutor(args, callbacks);
    });
  } else {
    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
  }
  }
});