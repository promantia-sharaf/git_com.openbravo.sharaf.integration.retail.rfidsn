/*
 ************************************************************************************
 * Copyright (C) 2017-2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global enyo, CUSTRFD */
(function () {
  enyo.kind({
    kind: 'OB.UI.SmallButton',
    name: 'OB.OBPOSPointOfSale.UI.EditLine.SnRfid',
    classes: 'btnlink-orange',
    events: {
      onShowPopup: ''
    },
    handlers: {
      onSetMultiSelected: 'setMultiSelected'
    },
    tap: function () {
      OB.MobileApp.view.waterfallDown('onShowPopup', {
        popup: 'CUSTRFD.UI.SerialNoRfid',
        args: {
          line: this.line,
          order: this.order
        }
      });
    },
    setMultiSelected: function (inSender, inEvent) {
      if (this.model.get('order').get('isEditable') && ((!OB.UTIL.isNullOrUndefined(inEvent.models[0].get('obposEpccode')) || !OB.UTIL.isNullOrUndefined(inEvent.models[0].get('obposSerialNumber'))) && (inEvent.models.length === 1) && (Math.abs(inEvent.models[0].getQty()) === 1) && (inEvent.models[0].get('cUSTDELDeliveryCondition') !== 'SD') && (inEvent.models[0].get('cUSTDELDeliveryCondition') !== 'WD'))) {
        this.line = inEvent.models[0];
        this.order = this.model.get('order');
        this.setShowing(true);
      } else {
        this.setShowing(false);
      }
    },
    init: function (model) {
      this.model = model;
      this.setContent(OB.I18N.getLabel('CUSTRFD_LblSNRFIDButton'));
    }
  });
  OB.OBPOSPointOfSale.UI.EditLine.prototype.actionButtons.push({
    kind: 'OB.OBPOSPointOfSale.UI.EditLine.SnRfid',
    name: 'snrfidButton'
  });
}());