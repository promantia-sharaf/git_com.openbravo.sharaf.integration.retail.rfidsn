/*
 ************************************************************************************
 * Copyright (C) 2017-2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global enyo, CUSTRFD */
enyo.kind({
  kind: 'OB.UI.ModalAction',
  name: 'CUSTRFD.UI.SerialNoRfid',
  topPosition: '60px',
  style: 'height:450px;',
  hideCloseButton: true,
  autoDismiss: false,
  closeOnEscKey: false,
  events: {
    onHideThisPopup: '',
    onShowPopup: ''
  },
  bodyContent: {
    components: [{
      style: 'border-bottom: 1px solid #cccccc; text-align:center; margin-bottom: 15px;',
      name: 'product'
    }, {
        kind: 'barcode',
        isFirstFocus: true
    }, {
      kind: 'serialNo',
    }, {
      kind: 'rfid'
    }]
  },
  bodyButtons: {
    components: [{
      kind: 'OB.UI.ModalDialogButton',
      name: 'ok',
      isDefaultAction: true,
      i18nContent: 'CUSTRFD_LblOkButton',
      tap: function () {
        this.owner.owner.actionApply();
      }
    }, {
      kind: 'OB.UI.ModalDialogButton',
      name: 'apply',
      isDefaultAction: true,
      i18nContent: 'CUSTRFD_LblClearButton',
      tap: function () {
        this.owner.owner.actionClear();
      }
    }, {
      kind: 'OB.UI.ModalDialogButton',
      name: 'cancel',
      isDefaultAction: true,
      i18nContent: 'OBPOS_Cancel',
      tap: function () {
        this.owner.owner.actionCancel();
      }
    }]
  },

  isBarcodeRequired: function () {
        if (!OB.UTIL.isNullOrUndefined(this.args.line.get('shaquoOriginalDocumentNo')) && !OB.MobileApp.model.get('terminal').custrfdEnablerfid) {
          return true;
        }
      return false;
    },

  actionApply: function () {
 // Focus on Serial No
    if (document.activeElement.id === this.$.bodyContent.$.barcode.$.barcodeInput.id) {
     this.$.bodyContent.$.serialNo.$.serialNoInput.focus();
     return;
    }
    // Focus on RFID Tag 
    if (document.activeElement.id === this.$.bodyContent.$.serialNo.$.serialNoInput.id) {
      this.$.bodyContent.$.rfid.$.rfidInput.focus();
      return;
    }

    var rfidCheck = null,
        serNoCheck = null,
        line = false;
    if (OB.UTIL.isNullOrUndefined(this.args.line)) {
      rfidCheck = this.args.data.productToAdd.get('custrfdIsRfidEnabled') && OB.MobileApp.model.get('terminal').custrfdOrgIsRfidEnabled;
      serNoCheck = this.args.data.productToAdd.get('custrfdIsSerialnoEnabled');
    } else {
      line = true;
      rfidCheck = this.args.line.get('product').get('custrfdIsRfidEnabled') && OB.MobileApp.model.get('terminal').custrfdOrgIsRfidEnabled;
      serNoCheck = this.args.line.get('product').get('custrfdIsSerialnoEnabled');
    }
    // Validate input
    var barcode = this.$.bodyContent.$.barcode.$.barcodeInput.getValue(),
     serialNo = this.$.bodyContent.$.serialNo.$.serialNoInput.getValue(),
        rfid = this.$.bodyContent.$.rfid.$.rfidInput.getValue();
    if (serNoCheck || serialNo) {
      if (!OB.CUSTRFD.Utils.isValidSerNo(serialNo)) {
        OB.UTIL.showConfirmation.display(OB.I18N.getLabel('CUSTRFD_LblError'), OB.I18N.getLabel('CUSTRFD_LblErrorSerialNo'));
        return;
      }
    }
    if (rfidCheck || rfid) {
      if (!OB.CUSTRFD.Utils.isValidRFID(rfid)) {
        OB.UTIL.showConfirmation.display(OB.I18N.getLabel('CUSTRFD_LblError'), OB.I18N.getLabel('CUSTRFD_LblErrorRfid'));
        return;
      }
    }
    if (rfidCheck || rfid) {
      var order = OB.UTIL.isNullOrUndefined(this.args.data) ? this.args.order : this.args.data.receipt;
      var lineId = OB.UTIL.isNullOrUndefined(this.args.line) ? '' : this.args.line.id;
      if (order.get('lines').length !== 0 && !OB.CUSTRFD.Utils.isUniqueRFID(rfid, order.get('lines'), lineId)) {
        OB.UTIL.showConfirmation.display(OB.I18N.getLabel('CUSTRFD_LblError'), OB.I18N.getLabel('CUSTRFD_UniqueErrorRfid'));
        return;
      }
    }
    
    
    // OBDEV-245 Validate duplicate Serial No
    
    if (serNoCheck && !OB.UTIL.isNullOrUndefined(OB.MobileApp.model.receipt.get('RestockingFee'))) {
      if (order.get('lines').length !== 0 && !OB.CUSTRFD.Utils.isUniqueSerialNo(serialNo, order.get('lines'), lineId)) {
        OB.UTIL.showConfirmation.display(OB.I18N.getLabel('CUSTRFD_LblError'), OB.I18N.getLabel('CUSTRFD_UniqueErrorSerialNo'));
        return;
      }
    }
    
    // End OBDEV-245
    
    
    var custrfd = {
      obposSerialNumber: serialNo,
      obposEpccode: rfid,
      obposBarcode: barcode
    };
    
    if (!OB.UTIL.isNullOrUndefined(this.args.order) && this.args.order.get('generatedFromQuotation') && this.isBarcodeRequired()){
 // check if line has upc linked - else return 
 // check if barcode is empty - if so show error 
      var upcCriteria = {
        upc: barcode
      };
      var me=this ;
      var upcFound = function (d) {
        // expect only one upc record to be returned. 
        if (d && d.length == 1) {
          if (d.models[0].attributes.m_product_id == me.args.line.attributes.product.attributes.id){
            console.log('Barcode matched');
            if (!me.args.line) {
              me.args.data.attrs.obposEpccode = custrfd.obposEpccode;
              me.args.data.attrs.obposSerialNumber = custrfd.obposSerialNumber;
              me.args.data.attrs.obposRfidValid = true;
              me.args.data.attrs.obposBarcode = custrfd.obposBarcode;
            } else {
              me.args.line.set('obposEpccode', rfid);
              me.args.line.set('obposSerialNumber', serialNo);
              me.args.line.set('obposRfidValid', true);
              me.args.line.set('obposBarcode', barcode);
            }
            me.doHideThisPopup();
            if (me.args.line) {
              me.args.order.save();
            }
            if (me.args.callback) {
              me.args.callback();
            }
          } else {
            console.log('Barcode does not match current Product');
            OB.UTIL.showConfirmation.display(OB.I18N.getLabel('CUSTRFD_LblError'), OB.I18N.getLabel('CUSTRFD_BarcodeProductMismatch'));
          }
        } else {
          console.log('Barcode not valid');
          OB.UTIL.showConfirmation.display(OB.I18N.getLabel('CUSTRFD_LblError'), OB.I18N.getLabel('CUSTRFD_InvalidBarcode'));
        }
     };
     OB.Dal.findUsingCache('multiupcSearch', OB.Model.MultiUPC, upcCriteria,upcFound, function(d) {
       OB.UTIL.showConfirmation.display(OB.I18N.getLabel('CUSTRFD_LblError'), OB.I18N.getLabel('CUSTRFD_BarcodeValidateUnexpectedError'));
     }) ; 
    }

    else {
      if (!line) {
         this.args.data.attrs.obposEpccode = custrfd.obposEpccode;
         this.args.data.attrs.obposSerialNumber = custrfd.obposSerialNumber;
         this.args.data.attrs.obposRfidValid = true;
         this.args.data.attrs.obposBarcode = custrfd.obposBarcode;
      } else {
         this.args.line.set('obposEpccode', rfid);
         this.args.line.set('obposSerialNumber', serialNo);
         this.args.line.set('obposRfidValid', true);
         this.args.line.set('obposBarcode', barcode);
      } 
      this.doHideThisPopup();
      if (line) {
          this.args.order.save();
      }
      if (this.args.callback) {
          this.args.callback();
      }
    }
   
  },
  actionClear: function () {
     if (!OB.UTIL.isNullOrUndefined(OB.MobileApp.model.receipt.get('generatedFromQuotation')) && OB.MobileApp.model.receipt.get('generatedFromQuotation')
         && this.isBarcodeRequired()) {
     this.$.bodyContent.$.barcode.$.barcodeInput.setValue('');
    }
    this.$.bodyContent.$.serialNo.$.serialNoInput.setValue('');
    this.$.bodyContent.$.rfid.$.rfidInput.setValue('');
  },
  actionCancel: function () {
    this.doHideThisPopup();
    if (!OB.UTIL.isNullOrUndefined(this.args.line)) {
      return;
    } else if (this.args.callback) {
      if(OB.MobileApp.model.get('terminal').custrfdEnablerfid) {
      	this.args.cancellation = true;
    	OB.CUSTRFD.Utils.processingQueue = false;
      } else {
      	this.args.callback();
      }
      
    }
  },
  executeOnShow: function () {
     
     // for 245 to hide candel button for open box sale
      var docType = OB.MobileApp.model.receipt.get('custsdtDocumenttypeSearchKey');
      var  restockingFeeDocType = (!OB.UTIL.isNullOrUndefined(OB.MobileApp.model.attributes.permissions.CPSRSF_RSFDocType) && 
            OB.MobileApp.model.attributes.permissions.CPSRSF_RSFDocType.includes(OB.MobileApp.model.receipt.get('custsdtDocumenttypeSearchKey').trim()))
            ?true : false;                            
      if(restockingFeeDocType && !OB.MobileApp.model.receipt.get('isVerifiedReturn')) {
        this.$.bodyButtons.$.cancel.hide();
      }
      // end 245
      
    if (this.args.line) {
      var product = this.args.line.get('product');
      var rsfsno = !OB.UTIL.isNullOrUndefined(product.get('obposSerialNumber')) ? product.get('obposSerialNumber') : '';
      this.$.bodyContent.$.product.setContent(product.get('_identifier'));
      if(this.args.order.get('generatedFromQuotation') && this.isBarcodeRequired() && !OB.MobileApp.model.get('terminal').custrfdEnablerfid){
         this.$.bodyContent.$.barcode.show();
         this.$.bodyContent.$.barcode.$.number.setContent(OB.I18N.getLabel('CUSTRFD_LblBarcode') + '*');
      } else {
 	 this.$.bodyContent.$.barcode.hide();
      }
      this.$.bodyContent.$.serialNo.$.number.setContent(OB.I18N.getLabel('CUSTRFD_LblSerialNo.') + (product.get('custrfdIsSerialnoEnabled') ? '*' : ''));
      this.$.bodyContent.$.rfid.$.rfidNo.setContent(OB.I18N.getLabel('CUSTRFD_LblRFID Tag') + (product.get('custrfdIsRfidEnabled') && OB.MobileApp.model.get('terminal').custrfdOrgIsRfidEnabled ? '*' : ''));
      this.$.bodyContent.$.barcode.$.barcodeInput.setValue(!OB.UTIL.isNullOrUndefined(this.args.line.get('obposBarcode')) ? this.args.line.get('obposBarcode') : '');
      this.$.bodyContent.$.serialNo.$.serialNoInput.setValue(!OB.UTIL.isNullOrUndefined(this.args.line.get('obposSerialNumber')) ? this.args.line.get('obposSerialNumber') : rsfsno);
      this.$.bodyContent.$.rfid.$.rfidInput.setValue(!OB.UTIL.isNullOrUndefined(this.args.line.get('obposEpccode')) ? this.args.line.get('obposEpccode') : '');
    } else {
      this.$.bodyContent.$.product.setContent(this.args.data.productToAdd.get('_identifier'));
      this.$.bodyContent.$.barcode.hide();
      this.$.bodyContent.$.serialNo.$.number.setContent(OB.I18N.getLabel('CUSTRFD_LblSerialNo.') + (this.args.data.productToAdd.get('custrfdIsSerialnoEnabled') ? '*' : ''));
      this.$.bodyContent.$.rfid.$.rfidNo.setContent(OB.I18N.getLabel('CUSTRFD_LblRFID Tag') + (this.args.data.productToAdd.get('custrfdIsRfidEnabled') && OB.MobileApp.model.get('terminal').custrfdOrgIsRfidEnabled ? '*' : ''));
      if (this.args.data.attrs && (!OB.UTIL.isNullOrUndefined(this.args.data.attrs.obposSerialNumber) || !OB.UTIL.isNullOrUndefined(this.args.data.attrs.obposEpccode))) {
        this.$.bodyContent.$.barcode.$.barcodeInput.setValue(OB.UTIL.isNullOrUndefined(this.args.data.attrs.obposBarcode) ? '' : this.args.data.attrs.obposBarcode);
     this.$.bodyContent.$.serialNo.$.serialNoInput.setValue(OB.UTIL.isNullOrUndefined(this.args.data.attrs.obposSerialNumber) ? '' : this.args.data.attrs.obposSerialNumber);
        this.$.bodyContent.$.rfid.$.rfidInput.setValue(OB.UTIL.isNullOrUndefined(this.args.data.attrs.obposEpccode) ? '' : this.args.data.attrs.obposEpccode);
      } else {
         // OBDEV-245 Openbox sale
        var sno = !OB.UTIL.isNullOrUndefined(this.args.data.productToAdd.get('obposSerialNumber')) ? this.args.data.productToAdd.get('obposSerialNumber') : '';
        this.$.bodyContent.$.barcode.$.barcodeInput.setValue('');
        this.$.bodyContent.$.serialNo.$.serialNoInput.setValue(sno);
        this.$.bodyContent.$.rfid.$.rfidInput.setValue('');
      }
    }
  },
  init: function () {
    this.inherited(arguments);
  }
});


enyo.kind({
   name: 'barcode',
   style: 'text-align: center;height: 80px;margin-left:5%;margin-right:5%;padding-bottom: 10px;',
   components: [{
     name: 'number',
     style: 'color:white;font-size:30px;font-weight:bold;'
   }, {
     name: 'barcodeInput',
     kind: 'enyo.Input',
     style: 'margin-top:10px;height: 25px;width: 80%;'
   }],
   focus: function () {
     this.$.barcodeInput.focus();
   },
   init: function () {
     this.inherited(arguments);
     this.$.number.setContent(OB.I18N.getLabel('CUSTRFD_LblBarcode'));
   }
});
enyo.kind({
  name: 'serialNo',
  style: 'text-align: center;height: 80px;margin-left:5%;margin-right:5%;padding-bottom: 10px;',
  components: [{
    name: 'number',
    style: 'color:white;font-size:30px;font-weight:bold;'
  }, {
    name: 'serialNoInput',
    kind: 'enyo.Input',
    style: 'margin-top:10px;height: 25px;width: 80%;'
  }],
  
  init: function () {
    this.inherited(arguments);
    this.$.number.setContent(OB.I18N.getLabel('CUSTRFD_LblSerialNo.'));
  }
});
enyo.kind({
  name: 'rfid',
  style: 'text-align: center;height: 80px;margin-left:5%;margin-right:5%;',
  components: [{
    name: 'rfidNo',
    style: 'color:white;font-size:30px;font-weight:bold;'
  }, {
    name: 'rfidInput',
    kind: 'enyo.Input',
    style: 'margin-top:10px;height: 25px;width: 80%;'
  }],
  initComponents: function () {
    this.inherited(arguments);
    this.$.rfidNo.setContent(OB.I18N.getLabel('CUSTRFD_LblRFID Tag'));
  }
});
OB.UI.WindowView.registerPopup('OB.OBPOSPointOfSale.UI.PointOfSale', {
  kind: 'CUSTRFD.UI.SerialNoRfid',
  name: 'CUSTRFD.UI.SerialNoRfid'
});
