/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, enyo, _ */

(function () {

  OB.CUSTRFD = OB.CUSTRFD || {};

  OB.CUSTRFD.Utils = {

    lineQueue: [],
    processingQueue: false,
    documentTypeSearchKey: ['SO', 'BS', 'BP', 'AR'],

    isValidDocumentType: function (docTypeSearchKey) {
      var key = _.find(this.documentTypeSearchKey, function (sk) {
        return sk === docTypeSearchKey;
      });
      return OB.UTIL.isNullOrUndefined(key);
    },

    isValidRFID: function (value) {
      if (value && value.length === 24) {
        return (value.substr(0, 3) === '2DD' || value.substr(0, 2) === 'AE');
      }
      return false;
    },

    isValidSerNo: function (value) {
      if (value && value !== '' && value.length !== 24 && value.length <= 240) {
        return (value.substr(0, 3) !== '2DD' && value.substr(0, 2) !== 'AE');
      }
      return false;
    },

    isUniqueRFID: function (rfid, lines, lineId) {
      var i = 0;
      for (i; i < lines.length; i++) {
        if (lines.models[i].get('obposEpccode') && lines.models[i].get('obposEpccode') === rfid && lines.models[i].id !== lineId) {
          return false;
        }
      }
      return true;
    },
    
    isUniqueRFIDImpl: function (rfid, lines) {
      var i = 0;
      for (i; i < lines.length; i++) {
        if (lines.models[i].get('obposEpccode') && lines.models[i].get('obposEpccode') === rfid) {
          return false;
        }
      }
      return true;
    },
    
    addSerialNoRfidLine: function (args, callbacks) {
      this.lineQueue.push({
        args: args,
        callbacks: callbacks
      });
      if (!this.processingQueue) {
        this.processingQueue = true;
        this.showSerialNoRfidPopup();
      }
    },

    finishCheckSerialNoRfidLines: function (order, lines, callback) {
      var ok = true;
      _.each(lines, function (lin) {
        if (!lin.checked) {
          ok = false;
        }
      });
      if (ok) {
        callback();
      } else {
        this.checkSerialNoRfidLines(order, lines, callback);
      }
    },


    checkSerialNoRfidLines: function (order, lines, callback) {
      var i, finishCallback = function () {
          OB.CUSTRFD.Utils.finishCheckSerialNoRfidLines(order, lines, callback);
          };
      for (i = 0; i < lines.length; i++) {
        if (!lines[i].checked) {
          lines[i].checked = true;
          OB.MobileApp.view.waterfallDown('onShowPopup', {
            popup: 'CUSTRFD.UI.SerialNoRfid',
            args: {
              order: order,
              line: lines[i].line,
              callback: finishCallback
            }
          });
          break;
        }
      }
    },

    showSerialNoRfidPopup: function () {
      var line = this.lineQueue.shift();
      if (!line.args.attrs) {
        line.args.attrs = {};
      }
      if (OB.UTIL.isNullOrUndefined(line.args.attrs.returnLineId)) {
        OB.MobileApp.view.waterfallDown('onShowPopup', {
          popup: 'CUSTRFD.UI.SerialNoRfid',
          args: {
            data: line.args,
            callback: function () {
              OB.UTIL.HookManager.callbackExecutor(line.args, line.callbacks);
              if (OB.CUSTRFD.Utils.lineQueue.length > 0) {
                OB.CUSTRFD.Utils.showSerialNoRfidPopup();
              } else {
                OB.CUSTRFD.Utils.processingQueue = false;
              }
            }
          }
        });
      } else {
        OB.UTIL.HookManager.callbackExecutor(line.args, line.callbacks);
        if (OB.CUSTRFD.Utils.lineQueue.length > 0) {
          OB.CUSTRFD.Utils.showSerialNoRfidPopup();
        } else {
          OB.CUSTRFD.Utils.processingQueue = false;
        }
      }
    },
   isUniqueSerialNo: function (seialNo, lines, lineId) {
      var i = 0;
      for (i; i < lines.length; i++) {
        if (lines.models[i].get('obposSerialNumber') && lines.models[i].get('obposSerialNumber') === seialNo && lines.models[i].id !== lineId) {
          return false;
        }
      }
      return true;
    },

  };

}());