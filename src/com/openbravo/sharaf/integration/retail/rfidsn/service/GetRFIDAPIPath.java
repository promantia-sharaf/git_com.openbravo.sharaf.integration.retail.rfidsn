package com.openbravo.sharaf.integration.retail.rfidsn.service;

import java.util.List;

import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.criterion.Restrictions;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.mobile.core.process.JSONProcessSimple;
import org.openbravo.model.common.enterprise.Organization;

import com.openbravo.sharaf.integration.retail.rfidsn.CustrfdRfidapiConfig;

public class GetRFIDAPIPath extends JSONProcessSimple {

  private static Logger log = Logger.getLogger(GetRFIDAPIPath.class);

  @Override
  public JSONObject exec(JSONObject jsonsent) throws JSONException, ServletException {

    JSONObject result = new JSONObject();
    JSONObject data = new JSONObject();
    String path = null;
    String org = jsonsent.getString("org");
    try {
      OBContext.setAdminMode(true);

      OBCriteria<CustrfdRfidapiConfig> obCriteriaPM = OBDal.getInstance()
          .createCriteria(CustrfdRfidapiConfig.class);

      obCriteriaPM.add(Restrictions.eq(CustrfdRfidapiConfig.PROPERTY_ORGANIZATION,
          OBDal.getInstance().get(Organization.class, "0")));

      List<CustrfdRfidapiConfig> lst = obCriteriaPM.list();

      if (!lst.isEmpty()) {
        if (lst.get(0).getRfidapipath() != null && lst.get(0).getRfidapipath() != "") {
          path = lst.get(0).getRfidapipath();
        }
      } else {

        OBCriteria<Organization> qOrgKey = OBDal.getInstance().createCriteria(Organization.class);
        qOrgKey.add(Restrictions.eq(Organization.PROPERTY_ID, org));
        Organization orgObj = qOrgKey.list().get(0);

        OBCriteria<CustrfdRfidapiConfig> obCriteria = OBDal.getInstance()
            .createCriteria(CustrfdRfidapiConfig.class);

        obCriteria.add(Restrictions.eq(CustrfdRfidapiConfig.PROPERTY_ORGANIZATION,
            OBDal.getInstance().get(Organization.class, orgObj.getId())));

        List<CustrfdRfidapiConfig> lt = obCriteria.list();

        if (!lt.isEmpty()) {
          if (lt.get(0).getRfidapipath() != null && lt.get(0).getRfidapipath() != "") {
            path = lt.get(0).getRfidapipath();
          }
        }

      }
      data.put("RFIDPath", path);
      result.put("status", 0);
      result.put("data", data);

    } catch (Exception e) {
      log.error("Error getting RFID API Path: " + e.getMessage());
      result.put("status", 1);
      result.put("message", e.getMessage());
    } finally {
      OBContext.restorePreviousMode();
    }
    return result;
  }

}
