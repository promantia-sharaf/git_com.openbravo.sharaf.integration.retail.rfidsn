/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package com.openbravo.sharaf.integration.retail.rfidsn.master;

import org.codehaus.jettison.json.JSONException;
import org.hibernate.criterion.Restrictions;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.retail.posterminal.CustomInitialValidation;
import org.openbravo.retail.posterminal.OBPOSApplications;

import com.openbravo.sharaf.integration.retail.rfidsn.CustrfdWebserviceConfig;

public class CustrfdLoginValidation extends CustomInitialValidation {

  @Override
  public void validation(OBPOSApplications posTerminal) throws JSONException {
    Organization organization = OBDal.getInstance().get(Organization.class,
        posTerminal.getOrganization().getId());
    if (organization.isCustrfdIsRfidEnabled()) {
      final OBCriteria<CustrfdWebserviceConfig> custrfdCriteria = OBDal.getInstance()
          .createCriteria(CustrfdWebserviceConfig.class);
      custrfdCriteria.add(Restrictions.eq("organization", organization));
      CustrfdWebserviceConfig custRfidConfig = (CustrfdWebserviceConfig) custrfdCriteria
          .uniqueResult();
      if (custRfidConfig == null) {
        throw new JSONException("CUSTRFD_LblErrorInitialValidation");
      }
    }
  }
}
