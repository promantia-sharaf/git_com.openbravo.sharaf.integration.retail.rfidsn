package com.openbravo.sharaf.integration.retail.rfidsn.master;

import java.util.ArrayList;
import java.util.List;

import org.openbravo.client.kernel.ComponentProvider.Qualifier;
import org.openbravo.mobile.core.model.HQLProperty;
import org.openbravo.mobile.core.model.ModelExtension;
import org.openbravo.retail.posterminal.master.Product;

@Qualifier(Product.productPropertyExtension)
public class CustrfdProductProperties extends ModelExtension {

  @Override
  public List<HQLProperty> getHQLProperties(Object params) {
    ArrayList<HQLProperty> list = new ArrayList<HQLProperty>();
    list.add(new HQLProperty("product.custrfdIsRfidEnabled", "custrfdIsRfidEnabled"));
    list.add(new HQLProperty("product.custrfdIsSerialnoEnabled", "custrfdIsSerialnoEnabled"));
    return list;
  }
}