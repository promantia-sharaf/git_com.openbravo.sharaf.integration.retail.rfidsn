/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package com.openbravo.sharaf.integration.retail.rfidsn.master;

import java.util.ArrayList;
import java.util.List;

import org.openbravo.client.kernel.ComponentProvider.Qualifier;
import org.openbravo.mobile.core.model.HQLProperty;
import org.openbravo.retail.posterminal.term.Terminal;

@Qualifier(Terminal.terminalPropertyExtension)
public class CustrfdTerminalProperties
    extends org.openbravo.retail.posterminal.term.TerminalProperties {
  @Override
  public List<HQLProperty> getHQLProperties(Object params) {
    ArrayList<HQLProperty> list = new ArrayList<HQLProperty>();
    list.add(new HQLProperty("pos.custrfdEnablerfid", "custrfdEnablerfid"));
    return list;
  }
}
