/*
 ************************************************************************************
 * Copyright (C) 2017-2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package com.openbravo.sharaf.integration.retail.rfidsn;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;

import org.openbravo.client.kernel.BaseComponentProvider;
import org.openbravo.client.kernel.BaseComponentProvider.ComponentResource.ComponentResourceType;
import org.openbravo.client.kernel.Component;
import org.openbravo.client.kernel.ComponentProvider;
import org.openbravo.retail.posterminal.POSUtils;

@ApplicationScoped
@ComponentProvider.Qualifier(CUSTRFDComponentProvider.CUSTRFD_COMPONENT_PROVIDER_QUALIFIER)
public class CUSTRFDComponentProvider extends BaseComponentProvider {

  public static final String CUSTRFD_COMPONENT_PROVIDER_QUALIFIER = "CUSTRFD_COMPONENT_PROVIDER_QUALIFIER";
  public static final String MODULE_JAVA_PACKAGE = "com.openbravo.sharaf.integration.retail.rfidsn";

  @Override
  public Component getComponent(String componentId, Map<String, Object> parameters) {
    throw new IllegalArgumentException("Component id " + componentId + " not supported.");
  }

  @Override
  public List<ComponentResource> getGlobalComponentResources() {
    final List<ComponentResource> globalResources = new ArrayList<ComponentResource>();
    final String prefix = "web/" + MODULE_JAVA_PACKAGE + "/js/";

    String[] resourceList = { "components/serialNoRfidPopup", "components/buttonSnRfid",
        "hooks/preAddProductHook", "hooks/preOrderSaveHook", "hooks/prePaymentHook",
        "hooks/OBPOS_BarcodeScan", "hooks/OBPOS_TerminalLoadedFromBackend",
        "hooks/OBPOS_PreAddProductHook", "model/addCustrfdProductProperties", "utils" };

    for (String resource : resourceList) {
      globalResources.add(createComponentResource(ComponentResourceType.Static,
          prefix + resource + ".js", POSUtils.APP_NAME));
    }

    return globalResources;
  }

  @Override
  public List<String> getTestResources() {
    return Collections.emptyList();
  }

}